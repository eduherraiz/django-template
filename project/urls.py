from django.conf.urls import patterns, include, url
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from admin import *
from frontend.views import *

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^robots\.txt$', lambda r: HttpResponse("", mimetype="text/plain")),
    url(r'', include('frontend.urls')),
    url(r'', include('social_auth.urls')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
