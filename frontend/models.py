# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from social_auth.models import UserSocialAuth
from datetime import datetime,timedelta
from django_fields.tests import EncryptedCharField
from django.core.mail import EmailMultiAlternatives
from django.utils.translation import ugettext as _
from django.shortcuts import render
from frontend.utils import send_email_mandrill, connect_tweepy
from django.template.loader import render_to_string
import twitter
import tweepy

#from django.db.models.signals import post_save
#from django.dispatch import receiver

# Define a custom User class to work with django-social-auth
class CustomUserManager(models.Manager):
    def create_user(self, username, email):
        return self.model._default_manager.create(username=username)


class CustomUser(models.Model):
    user = models.OneToOneField(UserSocialAuth)
    configured = models.BooleanField(default=False)
    username = models.CharField(max_length=128)
    photo = models.URLField(blank=True, null=True)
    language = models.CharField(max_length=128, default='en', blank=True)
    #timezone = models.CharField(max_length=128, default='Europe/Madrid', blank=True)
    #countrycode = models.CharField(max_length=4, default='ES', blank=True)

    last_login = models.DateTimeField(blank=True, null=True)
    
    email = models.EmailField(blank=True)
     
    objects = CustomUserManager()  
    
    counter = models.IntegerField(default=0)
    counter2 = models.IntegerField(default=0)
    
    def inc_counter(self):
        self.counter = self.counter + 1
        self.counter2 = self.counter2 + 2 
        self.save()
        
    def update_login_date(self):
        ##Checkin login in last day to update last_login, used in tasks
        if not self.last_login:
            self.last_login = datetime.utcnow()
            self.save()
            
    def update_twitter_status(self, text):
        if text:
            api = connect_tweepy(self.user)
            api.update_status(text)

    #def update_twitter_photo(self):
        #api = connect_tweepy(self.user)
        #user = api.get_user(self.username)
        #self.photo = user.profile_image_url
        #self.save()
        
    def admin_thumbnail(self):
        return u'<img src="%s" />' % (self.photo)

    admin_thumbnail.short_description = 'Thumbnail'
    admin_thumbnail.allow_tags = True
       
    def is_authenticated(self):
        return True
    
    def __unicode__(self):
        return self.username
