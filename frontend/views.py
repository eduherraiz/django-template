from django.http import HttpResponseRedirect
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib.messages.api import get_messages
from django.conf import settings
from django.utils.translation import get_language
from django.utils.translation import ugettext as _
from datetime import datetime,timedelta

from tweepy.error import TweepError

from social_auth import __version__ as version
from social_auth.utils import setting
from social_auth.models import UserSocialAuth

from frontend.models import CustomUser
from frontend.forms import *
from django.core.mail import EmailMessage

def get_user(request):
    try: 
        instance = UserSocialAuth.objects.filter(user=request.user.id).get()
    except UserSocialAuth.DoesNotExist:
        return None
    try:
        profile = CustomUser.objects.filter(user=instance).get()
    except: #Not user defined 
        pass #Always defined by the login pipeline
    
    profile.update_login_date()
    return profile
    
def home(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        return HttpResponseRedirect('done')
    else:
        return render_to_response('home.html', {'version': version}, RequestContext(request))

def contact(request):
    from_email = ""
    sended = failed = False
    
    if request.method == 'POST': # If the form has been submitted...
        form = ContactForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            #Send the email
            subject = form.cleaned_data['subject']
            html_content = form.cleaned_data['message']
            from_email = form.cleaned_data['sender']
            from_name = form.cleaned_data['name']
            to = settings.CONTACT_EMAIL
            msg = EmailMessage(subject, html_content, to, [to])
            msg.content_subtype = "html"  # Main content is now text/html
            msg.send()
            sended = True
        else:
            failed = True
    else:
        form = ContactForm() # An unbound form

        
    ctx = {
        'form': form,
        'from_email' : from_email,
        'sended' : sended,
        'failed': failed,
    }
        
    return render_to_response('contact.html', ctx, RequestContext(request))
    
def about(request):
    """Home view, displays login mechanism"""
    return render_to_response('about.html', {'version': version}, RequestContext(request))
    
#@login_required
#def config(request):
    #"""Login complete view, displays user data"""
    #user = get_user(request.user)
    ##ip = request.META.get('REMOTE_ADDR', None)
    ##countrycode = get_possible_country_code(ip)
    ##timezones = country_timezones(countrycode)
       
    #saved = False
        
    #if request.method == 'POST': # If the form has been submitted...
        #form = ConfigForm(request.POST) # A form bound to the POST data
        #if form.is_valid(): # All validation rules pass
            ##Save the user config in the table
            #user.username = request.user.username
            #user.email = form.cleaned_data['email']
                
            #user.language = get_language()
            #user.update_twitter_photo()
            #user.save()

            
            #if not user.configured:
                ##user.update_date() #Not necessary yet, updated on first save
                #user.configured = True
                #user.save()
            #saved = True
    #else:
        #form = ConfigForm(instance=user) # An unbound form

        
    #ctx = {
        #'form': form,
        #'user': user,
        #'saved' : saved,
        ##'timezones' : timezones,
    #}
    
    #return render_to_response('config.html', ctx, RequestContext(request))
    
@login_required
def done(request):
    """Login complete view, displays user data"""
    profile = get_user(request)
    ctx = {
        'version': version,
        'last_login': request.session.get('social_auth_last_login_backend'),
        'profile': profile,
    }
    return render_to_response('done.html', ctx, RequestContext(request))
    
def error(request):
    """Error view"""
    messages = get_messages(request)
    return render_to_response('error.html', {'version': version,
                                             'messages': messages},
                              RequestContext(request))

def logout(request):
    """Logs out user"""
    auth_logout(request)
    return HttpResponseRedirect('/')