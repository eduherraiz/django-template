 #-*- coding: UTF-8 -*-
from django.forms import ModelForm, Textarea, Form, CharField, EmailField
from django.utils.translation import ugettext_lazy as _
from frontend.models import CustomUser

class ContactForm(Form):
    subject = CharField(label=_("Message subject"),max_length=200,required=True)
    message = CharField(label=_("Your message"),widget=Textarea(),required=True )
    sender = EmailField(label=_("Email address"), required=True)
    name = CharField(label=_("Your name or company"),max_length=200,required=True)
    #cc_myself = forms.BooleanField(required=False) 

class ConfigForm(ModelForm):
    class Meta:
        model = CustomUser
        fields = ('email',)
        #widgets = {
            #'text': Textarea(attrs={'cols': 40, 'rows': 5}),
        #}       
 