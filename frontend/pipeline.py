from django.http import HttpResponseRedirect
from social_auth.models import UserSocialAuth
from frontend.models import CustomUser
from social_auth.backends.facebook import FacebookBackend
from social_auth.backends.twitter import TwitterBackend

def create_profile(backend, request, user, response, details, is_new=False, *args, **kwargs): 

    #backend = request.session.get('social_auth_last_login_backend')
    
    instance = UserSocialAuth.objects.filter(user=user.id).get()
    
    if is_new: 
        profile = CustomUser.objects.create(user=instance, username=user.username)
    else:
        profile = CustomUser.objects.filter(user=instance).get()
         
    if backend.__class__ == FacebookBackend:
        profile.photo = "http://graph.facebook.com/%s/picture?type=large" % response['id']
    elif backend.__class__ == TwitterBackend:
        profile.photo = response.get('profile_image_url', '').replace('_normal', '')
        
    profile.save()

        

#def get_user_avatar(backend, details, response, social_user, uid, user, *args, **kwargs):
    
    #instance = UserSocialAuth.objects.filter(provider=request.session.get('social_auth_last_login_backend'),user=request.user).get()
    #user = CustomUser.objects.filter(user=instance).get()
    
    #url = None

        #url = "http://graph.facebook.com/%s/picture?type=large" % response['id']
 
    #elif backend.__class__ == TwitterBackend:
        #url = response.get('profile_image_url', '').replace('_normal', '')
 
    #if url:
        #user.photo = url
        #user.save()
        #profile = user.get_profile()
        #avatar = urlopen(url).read()
        #fout = open(filepath, "wb") #filepath is where to save the image
        #fout.write(avatar)
        #fout.close()
        #profile.photo = url_to_image # depends on where you saved it
        #profile.save()