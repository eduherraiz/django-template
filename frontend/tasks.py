from celery import task
from frontend.models import CustomUser
from datetime import timedelta

@task()
def counter():
    logger = counter.get_logger(logfile='tasks.log')
    users = CustomUser.objects.all()
    
    for user in users:
        user.inc_counter()
        logger.info('Increase counter: %s (%s)' % (user.username, user.counter))




