from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import ugettext_lazy as _


## Uncomment the next two lines to enable the admin:
from django.contrib import admin

from frontend.views import *
from frontend.facebook import facebook_view

urlpatterns = patterns('',
    url(r'^contact/$', contact, name='contact'),
    url(r'^about/$', about, name='about'),
    url(r'^fb/', facebook_view, name='fb_app'),
    url(r'^done/$', done, name='done'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^$', home, name='home'),
    #url(r'^error/$', error, name='error'),
    #url(r'^config/$', config, name='config'),
)
