# -*- coding: utf-8 -*-
#!/usr/bin/python

from __future__ import with_statement
from fabric.api import *
from fabric.contrib.console import confirm
from fabric.colors import *
from datetime import datetime as _date
from project.settings import *

def install_requeriments():
    #local("sudo apt-get install redis-server")
    local("/home/edu/.bashrc && workon %s && pip install -r requeriments.txt" % PROJECT_NAME)
    
def update_pip_requeriments():
    # Se podrían actualizar todos los paquetes instalados con el siguiente comando
    local("pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs pip install -U")
    
def install_pip_requeriments():
    with prefix('WORKON_HOME=$HOME/virtualenvs'):
        with prefix('source /usr/local/bin/virtualenvwrapper.sh'):
            with prefix('workon proyecto'): 
                run('pip install -r requirements.txt') 